# coding=utf-8
from atlassian import Bitbucket
import json


bitbucket = Bitbucket(
    url='http://localhost:7990',
    username='sujith',
    password='sujith')

data = bitbucket.update_repo("DEV", "repo1", description="Auto update from CLI")
json_string = json.dumps(data, indent=2)
print (json_string)