# coding=utf-8
from atlassian import Bitbucket

bitbucket = Bitbucket(
    url='http://localhost:7990',
    username='sujith',
    password='sujith')


#https://atlassian-python-api.readthedocs.io/bitbucket.html#groups-and-admins
#https://stackoverflow.com/questions/43627994/how-to-create-a-branch-using-bitbucket-rest-api

bitbucket.create_branch("DEV", "repo1", "feature1", "refs/heads/master")
