# coding=utf-8
from atlassian import Bitbucket
import json

bitbucket = Bitbucket(
    url='http://localhost:7990',
    username='sujith',
    password='sujith')


#https://atlassian-python-api.readthedocs.io/bitbucket.html#groups-and-admins
#https://stackoverflow.com/questions/43627994/how-to-create-a-branch-using-bitbucket-rest-api

data = json.dumps(bitbucket.get_users(user_filter=""), indent=2)

print (data)
